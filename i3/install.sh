# Clean workspace
rm -rf contrib i3lock-color i3lock-fancy light polybar i3-gaps i3-blocks
# Clone repos
git clone https://github.com/vivien/i3blocks-contrib.git contrib
git clone https://github.com/PandorasFox/i3lock-color.git i3lock-color
git clone https://github.com/meskarune/i3lock-fancy.git i3lock-fancy
git clone https://github.com/haikarainen/light.git light
git clone --recursive https://github.com/jaagr/polybar polybar
git clone https://www.github.com/Airblader/i3 i3-gaps
git clone https://github.com/Airblader/i3blocks-gaps.git i3blocks

# Build i3lock
cd i3lock-color && autoreconf -i && ./configure && make && sudo make install && cd ..
# Build light
cd light && ./autogen.sh && ./configure && make && sudo make install && cd ..
# Build xwinwrap
##cd xwinwrap && make && sudo make install && cd ..
# Build polybar
mkdir -p polybar/build && cd polybar/build && cmake .. && sudo make install && cd ../../
# Build i3-gaps
cd i3-gaps && autoreconf --force --install && rm -rf build && mkdir -p build && cd build && ../configure --prefix=/usr --sysconfdir=/etc --disable-sanitizers && make && sudo make install && cd ../../
# Build i3-blocks
cd i3blocks && make clean all && make && sudo make install

# Inject config
rm -rf ~/.config/i3
ln -sfT $(pwd) ~/.config/i3
