def purple():
    return "#bb77bb"

def gold():
    return "#ffee88"

def red():
    return "#bb5566"

def blue():
    return "#8899ff"

def green():
    return "#aaff99"

def brown():
    return "#f08355"

def silver():
    return "#ccccff"

def darkblue():
    return "#586e75"

def color(text,col):
    return "%%{F%s}%s%%{F-}" % (col,text)

def underline(col,bot = True):
    string = "u" if bot else "o"
    return "%%{%s%s +%s}" % (string,col,string)

def nounderline(bot = True):
    string = "u" if bot else "o"
    return "%%{%s-}" % string

def action(text,command):
    return "%%{A1:%s:}%s%%{A}" % (command,text)
