#!/bin/bash

for d in  */;
do
	cd $d
	printf "%-35s %-8s %-1s" "Running installation script inside" $d "... "

	eval "./install.sh" && printf "%-10s\n" "success!" || printf "%-10s\n" "failed!"

	cd "../"
done

