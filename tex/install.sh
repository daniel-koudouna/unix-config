LOC=$(pwd)

rm -rf ~/.config/tex
mkdir -p ~/.config/
ln -sfT $(pwd) ~/.config/tex

LINE1="export TEX_CONFIG=$LOC"
LINE2="alias pull-tex=\"cp ~/.config/tex/saint.tex ./saint.tex &&
touch bibliography.bib &&
mkdir images &&
cp ~/.config/tex/images/coat-of-arms.png images/coat-of-arms.png &&
cp ~/.config/tex/report.tex ./report.tex\""
grep -qF "$LINE1" ~/.profile || echo "$LINE1" >> ~/.profile
grep -qF "$LINE2" ~/.profile || echo "$LINE2" >> ~/.profile
