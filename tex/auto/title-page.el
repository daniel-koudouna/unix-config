(TeX-add-style-hook
 "title-page"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("color" "usenames" "dvipsnames")))
   (TeX-run-style-hooks
    "fancyhdr"
    "lastpage"
    "extramarks"
    "color"
    "graphicx"
    "listings"
    "courier"
    "lipsum"
    "float"
    "url"
    "amssymb"
    "pgfplots"
    "hyperref")
   (TeX-add-symbols
    "coverpage"))
 :latex)

