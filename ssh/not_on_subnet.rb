require 'optparse'
require 'ipaddr'
require 'socket'

opts = OptionParser.new do |o|
  o.banner = "Usage: #{File.basename($0)} [-v] subnet [subnet...]"
  o.on_tail("-h", "--help", "Show this message") do
    puts o
    exit
  end
end
opts.parse!

if ARGV.empty?
  
  # Display help message and exit
  puts opts
  exit 64
  
else

  # Enumerate subnets passed on command line
  subnets = []
  ARGV.each do |subnet|
    begin
      subnets << IPAddr.new(subnet)
    rescue IPAddr::InvalidAddressError
      STDERR.puts "'#{subnet}' is not a valid subnet"
      exit 65
    end
  end
  
  # Enumerate interface IP addresses
  Socket.ip_address_list.each do |addrinfo|
    subnets.each do |subnet|
      begin
        exit 1 if subnet.include?(addrinfo.ip_address)
      rescue IPAddr::InvalidAddressError
        # NOP
      end
    end
  end

end
  
# No on any of the passed subnet
exit 0

