python3 -m venv .
chmod +x bin/activate
. bin/activate
pip install --user --upgrade pip
pip install -r requirements.txt

rm -rf ~/.config/pythonenv
ln -sfT $(pwd) ~/.config/pythonenv

mkdir -p ~/bin
ln -sfT ~/.config/pythonenv/run.sh ~/bin/pythonenv
chmod +x ~/bin/pythonenv
