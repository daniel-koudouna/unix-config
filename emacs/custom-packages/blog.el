;;; blog --- Dynamic configuration for org-publish
;;; Commentary:
;;; Apply all relevant settings for org-publish, to configure custom
;;; elements such as header, footer, index etc.
;;; Code:

;; Allow closures
(setq lexical-binding t)

;; Add custom export options
(add-to-list 'org-export-options-alist '(:category-name "CATEGORY_NAME" nil "Home" t))
(add-to-list 'org-export-options-alist '(:root "ROOT" nil nil t))
(add-to-list 'org-export-options-alist '(:quote "QUOTE" nil nil t))
(add-to-list 'org-export-options-alist '(:source "QUOTE_SOURCE" nil nil t))
(add-to-list 'org-export-options-alist '(:banner "BANNER" nil nil t))
(add-to-list 'org-export-options-alist '(:index-for "INDEX_FOR" nil nil t))
(add-to-list 'org-export-options-alist '(:index-limit "INDEX_LIMIT" nil nil t))

(defun my:trim-string (string)
  "Remove white spaces in beginning and ending of STRING.
White space here is any of: space, tab, emacs newline (line feed, ASCII 10)."
(replace-regexp-in-string "\\`[ \t\n]*" "" (replace-regexp-in-string "[ \t\n]*\\'" "" string))
)

(defmacro my:with-helpers (options)
  "Create helper functions for OPTIONS."
  `(progn
     (defun my:getd (kw default)
       (let ((val (plist-get ,options kw)))
	 (if val
	     (car val)
	   default)))
     (defun my:get (kw)
       (my:getd kw "DEFAULT"))

     (defun my:getr (kw)
       (plist-get ,options kw))

     (defun my:getdr (kw default)
       (let ((val (plist-get ,options kw)))
	 (if val
	     val
	   default)))

     (defun my:rel (link)
       (concat (my:getdr ':root "../") link))

     )
  )

(defun my:get-type (link)
  (let ((i (string-match "/" link)))
    (if i
	(substring link 0 i)
      "")))

(defun my:read-lines (path)
  (with-temp-buffer
    (insert-file-contents path)
    (split-string (buffer-string) "\n" t)))

(defun my:get-option (path option default)
  (let ((title-line (car (seq-filter
			  (lambda (line)
			    (string-prefix-p option line)
			    )
			  (my:read-lines path)))))
    (if title-line
	(my:trim-string (substring title-line (+ 1 (string-match ":" title-line))))
      default)
    ))

(defun my:entry-to-html (entry)
  (concat "<hr>"
          "<div class='index-post'>"
	  "<a class='index-content' href='" (alist-get :link entry) "'>"
	  "<div class='index-left'>"
          "<h3 class='list-title'>"
          "<div>"(alist-get :title entry)"</div>"
          "<div>&#9679;</div>"
	  "<div class='list-date'>" (alist-get :pretty-date entry) "</div>"
          "</h3>"
          "<div class='list-quote'>" (alist-get :quote entry) "</div>"
	  "</div>"
	  "<div class='index-right'>"
	  "<img class='index-banner' src='" (my:rel (alist-get :banner entry)) "'>"
	  "</div>"
	  "</a>"
	  "</div>"))

(defun my:read-org-date (date)
  (split-string (org-read-date nil nil date) "-"))

(defun my:current-org-date ()
  (format-time-string "<%Y-%m-%d %a>"))

(defun my:compare-entries (a1 a2)
  (string> (alist-get :date a1) (alist-get :date a2)))

(defun my:link-title (link)
  (file-name-sans-extension (file-name-nondirectory link)))

(defun my:link-to-entry (root)
  (lexical-let
      ((root root))
    (lambda (link)
      (let* ((rel-path (file-relative-name link root))
	     (rel-link (replace-regexp-in-string "\\.org" "\.html" rel-path))
	     (type (my:get-type rel-link))
	     (quotation (my:get-option link "#+QUOTE:" ""))
	     (link-title (my:get-option link "#+TITLE:" (my:link-title link)))
	     (date (my:get-option link "#+DATE:" (my:current-org-date)))
	     (banner (my:get-option link "#+BANNER:" "img/default.png")))
	`((:path . ,rel-path)
	  (:link . ,rel-link)
	  (:type . ,type)
	  (:title . ,link-title)
	  (:banner . ,banner)
	  (:date . ,date)
          (:quote . ,quotation)
	  (:pretty-date . ,(my:prettify-date date)))
	))))

(defun my:get-entry-year (entry)
  (let* ((l (parse-time-string (alist-get :date entry)))
	 (y (nth 5 l)))
    (number-to-string y)))

(defun my:month-str (m)
  (pcase m
    (1 "Jan")
    (2 "Feb")
    (3 "Mar")
    (4 "Apr")
    (5 "May")
    (6 "Jun")
    (7 "Jul")
    (8 "Aug")
    (9 "Sep")
    (10 "Oct")
    (11 "Nov")
    (12 "Dec")))

(defun my:prettify-date (date)
  (let* ((l (parse-time-string date))
	 (w (nth 6 l))
	 (y (nth 5 l))
	 (m (nth 4 l))
	 (d (nth 3 l)))
    (format "%s %02d, %d" (my:month-str m) d y)))

(defun my:create-root-index (base title)
  "Create index page using custom properties starting from BASE."
  (lexical-let
      ((root base)
       (title title))
    (lambda (options)
      (my:with-helpers options)

      (let* ((subfolders (split-string (my:getdr ':index-for "") " ")))
	(concat
	 "<a class='site-title' "
	 "href='" (my:rel (my:getr ':html-link-home)) "'>"
         title
	 "</a><br>"
	 (mapconcat
	  (lambda (subfolder)
	    (let* ((idir (concat root subfolder))
		   (links (directory-files-recursively idir "\.org"))
		   (entries (sort (mapcar (my:link-to-entry root) links) 'my:compare-entries))
		   (ilimit (my:getdr ':index-limit nil))
		   (year-entries (seq-group-by 'my:get-entry-year entries))
		   (limited-entries (if ilimit (list (cons "Latest" (seq-take entries (string-to-number ilimit)))) nil))
		   )
	      (concat
	       "<div class='index-block'>"
	       (mapconcat
		(lambda (es)
		  (concat
                   "<h3 class='index-header'>"
                   "<a href='" (my:rel subfolder) ".html'>" (capitalize subfolder) "</a>"
                   " / "
                   (car es)
                   "</h3>"
		   (mapconcat
		    'my:entry-to-html
		    (cdr es)
		    "")))
		(if limited-entries limited-entries year-entries)
		"")
               "</div>"
	       )
	      )
	    )
	  subfolders
	  "")
	 )
	)
      )
    ))


(defun my:website-html-preamble (options)
  "Create custom preamble using OPTIONS."
  (my:with-helpers options)

  (concat "<a class='site-title' "
	  "href='" (my:rel (my:getr ':html-link-home)) "'>"
	  "The Velvet Room"
	  "</a><br>"
	  "<h3 class='index-header'>"
	  "<a class='header-link' href='" (my:rel (my:getr ':html-link-up)) "'>"
          (my:getr ':category-name)
	  "</a> / <a class='header-link' href='#'>" (my:get ':title) "</a>"
	  "</h3>"
	  "<hr>"
          "<div class='banner-block'>"
	  "<img class='banner' src='" (my:rel (my:getdr ':banner "img/default.png")) "'>"
	  "<div class='banner-quote'>"
	  "<p>" (my:getdr ':quote "") "</p>"
	  "<p>" (my:getdr ':source "") "</p>"
          "</div>"
          "</div>"
	  "<hr>"
	  )
  ) ;; preamble


(defun my:website-html-postamble (options)
  "Create custom posamble using OPTIONS."
  (my:with-helpers options)

  (concat "<hr>"
	  (if (and (my:getr ':keywords) (not (string= (my:getr ':keywords) "")))
	      (format "<div class='footer-tags'><p>%s</p></div>"
		      (mapconcat
		       (function (lambda (s) (concat " :<a href='#'>" s "</a>")))
		       (delete "" (split-string (my:getr ':keywords)))
		       ""))
	    "<div class='footer-tags'></div>")

	  (format "<div class='footer-author'><p>%s</p></div>" (format-time-string "%b %d, %Y"))
	  (format "<div class='footer-version'>%s</div>" (my:getr ':creator))
	  "</div>"

	  )
  ) ;; postamble

(defun my:website-head (rel)
  (concat "<link rel=\"stylesheet\" type=\"text/css\" href=\""
	  rel "css/theme.css\" />"
          "<link href=\"https://fonts.googleapis.com/css?family=Noto+Sans\" rel=\"stylesheet\">"
          "<link href=\"https://fonts.googleapis.com/css?family=Noto+Sans+Mono\" rel=\"stylesheet\">"
          ))

(defun my:file-tuples (root n)
  (mapcar
   (lambda (s) `(,s ,n))
   (directory-files root t "[a-zA-Z]+.*"))
  )

(defun my:dirs (tups)
  (seq-filter (lambda (x) (file-directory-p (car x))) tups))

(defun my:increment (tup)
  (+ 1 (car (cdr tup))))

(defun my:uniq (l)
  (delq nil (delete-dups l)))

(defun my:flatmap (l)
  (apply #'append l))

(defun my:subdirs (tup)
  (my:dirs (my:file-tuples (car tup) (my:increment tup))))

(defun my:more-dirs (ds)
  (my:uniq
   (my:flatmap
    (mapcar
     (lambda (tup)
       (append `(,tup) (my:subdirs tup)))
     ds)
    )))

(defun my:recursive-directories (base)
  (setq web-counter 1)
  (setq web-dirs (my:dirs (my:file-tuples base 1)))
  (while (< web-counter 4)
    (setq web-dirs (my:more-dirs web-dirs))
    (setq web-counter (+ 1 web-counter)))
  web-dirs
  )

(defun my:is-post (tup)
  (not (or
	(string-match "font" (car tup))
	(string-match "css" (car tup))
	(string-match "img" (car tup)))))

(defun my:prev (n)
  (if (<= n 0)
      ""
    (concat "../" (my:prev (- n 1)))))

(defun my:compile-site-list (base dest name title)
  (let* ((web-dirs (my:recursive-directories base))
	 (post-dirs (seq-filter 'my:is-post web-dirs))
	 (temp-alist
	  (list
	   ;; Indices
	   `( ,(concat name "-root")
	      :base-directory ,base
	      :base-extension "org"
	      :publishing-directory ,dest
	      :recursive nil
	      :publishing-function org-html-publish-to-html
	      :auto-sitemap nil
	      :root "./"
	      :html-head ,(my:website-head "./")
	      :html-link-home "index.html"
	      :html-head-include-default-style nil ;no default css
	      :html-preamble ,(my:create-root-index base title)
	      :html-postamble my:website-html-postamble
	      )
	   ;; Static assets
	   `( ,(concat name "-static-assets")
	      :base-directory ,base
	      :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|ttf"
	      :publishing-directory ,dest
	      :recursive t
	      :publishing-function org-publish-attachment
	      )))
	 (all-names
	  (list (concat name "-static-assets") (concat name "-root")))
	 (temp-palist
	  (mapcar
	   (lambda (tup)
	     (let* ((dir (car tup))
		    (absolute-base (expand-file-name base))
		    (dirname (file-relative-name dir absolute-base))
		    (n (car (cdr tup)))
		    (task-name (replace-regexp-in-string "/" "-" (concat name "-" dirname)))
		    (trail (my:prev n)))
	       (add-to-list 'all-names task-name)

	       `( ,task-name
		  :base-directory ,(concat dir "/")
		  :base-extension "org"
		  :publishing-directory ,(concat dest dirname "/")
		  :recursive nil
		  :publishing-function org-html-publish-to-html
		  :auto-sitemap nil
		  :root ,trail
		  :html-preamble my:website-html-preamble
		  :html-postamble my:website-html-postamble
		  :html-head ,(my:website-head trail)
		  :html-head-include-default-style nil ;no default css
		  )
	       ))
	   post-dirs))
	 (temp-mlist (list `(,name :components ,all-names)) ))
    (append temp-alist temp-palist temp-mlist)
    ))

(setq org-publish-project-alist (my:compile-site-list "~/org/blog/" "~/public_html/velvet/" "velvet" "The Velvet Room"))
