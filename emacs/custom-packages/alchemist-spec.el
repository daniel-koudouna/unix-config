;;; alchemist-spec.el --- Functionality to display typespecs for Elixir functions. -*- lexical-binding: t -*-

;; Copyright © 2015 Daniel Koudouna

;; Author: Daniel Koudouna <daniel.koudouna@gmail.com>

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;; Functionality to display typespecs for Elixir functions.

;;; Code:
(require 'cl-lib)
(require 'etags)
(require 'dash)
(require 's)
(require 'alchemist-utils)
(require 'alchemist-server)
(require 'alchemist-scope)


(defun my-map-remove-null (fn list)
  "Remove null and map FN to a LIST."
  (-remove 'null (-map fn list)))

(defun my-trim-newline (s)
  "Remove trailing newline from S."
  (replace-regexp-in-string "\n$" "" s))

(defun my-alchemist-find-all-symbol-markers (symbol)
  "Find the names and positions of a given SYMBOL."
  (my-map-remove-null
   (lambda (e) (when (string= symbol (car e)) e))
   alchemist-goto--symbol-name-and-pos-bare))

(defun my-spec-show (s)
  "Prints S to the minibuffer unless it was the last thing printed."
  (if (not (string= s my-spec-current-expression))
      (let ((message-log-max nil))
	(message "%s" s)))
  (setq my-spec-current-expression s))

(defun my-reduce-newline (a b)
  "Concat A and B followed by newline."
  (concat a (concat b "\n")))

(defun my-join-newline (list)
  "Convenient reduction of LIST by newline."
  (seq-reduce 'my-reduce-newline list ""))

(defun my-alchemist-search-buffer-spec (marker name)
  "Search above a MARKER for a spec matching NAME."
  (save-excursion
    (goto-char marker)
    (search-backward "@spec " nil "move-if-fail")
    (if
	(cl-search (concat "@spec " name) (thing-at-point 'line))
	(progn
	  (beginning-of-line)
	  (let ((curline (point)))
	    (search-forward (concat "def " name) nil nil)
	    (forward-line -1)
	    (end-of-line)
	    (my-trim-newline (buffer-substring curline (point)))
	    ))
      nil)))


(defun my-alchemist-find-spec-single (pair)
  "Find a single specification for a PAIR of name / location."
  (let*
      ((name (car pair))
       (marker (cdr pair))
       (buffer (marker-buffer marker)))
    (with-current-buffer buffer
      (my-alchemist-search-buffer-spec marker name))))

(defun my-alchemist-get-specs (markers)
  "Find typespecs for all pairs of MARKERS."
  (my-trim-newline
   (my-join-newline
    (seq-uniq
     (my-map-remove-null
      'my-alchemist-find-spec-single
      markers)))))

(defun my-alchemist-spec-get-elixir-source (module function)
  (goto-char (point-min))
  (cond
   (function
    (alchemist-goto--fetch-symbol-definitions)
    ;;(format "GOING TO %s" function)
    (my-alchemist-get-specs (my-alchemist-find-all-symbol-markers function))			;	  (alchemist-goto--goto-symbol function))
    )
   (t
    (when (re-search-forward (format "\\(defmodule\\|defimpl\\|defprotocol\\)\s+%s\s+do" module) nil t)
      (goto-char (match-beginning 0))
      "what are you searching?"
      )
    )))

(defun my-alchemist-spec-from-file (file module function)
  (let ((buf (find-file-noselect file)))
    (save-excursion
      (with-current-buffer buf
	(cond ((alchemist-goto-elixir-file-p file)
	       (my-alchemist-spec-get-elixir-source module function))
	      ((alchemist-goto-erlang-file-p file)
	       "NOT  IMPLEMENTED"))
	))))

(defun my-alchemist-display-definition (expr)
  "Display a message for the typespec of EXPR, if it exists."
  (if (assoc expr my-spec-cache)
      (my-spec-show (cdr (assoc expr my-spec-cache)))
    (let* ((module (alchemist-scope-extract-module expr))
	   (aliases (alchemist-utils-prepare-aliases-for-elixir
		     (alchemist-scope-aliases)))
	   (function (alchemist-scope-extract-function expr))
	   (modules (alchemist-utils-prepare-modules-for-elixir
		     (alchemist-scope-all-modules))))
      (ring-insert find-tag-marker-ring (point-marker))
      (cond
       ((and (null module)
	     (alchemist-goto--symbol-definition-p function)
	     (not (string= (buffer-name) alchemist-help-buffer-name)))
	(my-spec-show (my-alchemist-get-specs (my-alchemist-find-all-symbol-markers function))))
       (t

	(setq lexical-binding t)
	(setq alchemist-goto-callback
	      (lambda (file)
		(cond ((s-blank? file) "")
		      ((file-exists-p file)
					;(alchemist-goto--open-file file module function))
		       (my-spec-show (format "FILE EXISTS %s %s %s" (or file "nil") (or module "nil") (or function "nil"))))
		      ((alchemist-goto-elixir-file-p file)
		       (let* ((elixir-source-file (alchemist-goto--build-elixir-ex-core-file file)))
			 (if (and elixir-source-file (file-exists-p elixir-source-file))
			     (progn
			       (add-to-list 'my-spec-cache `(,expr . ,(my-alchemist-spec-from-file elixir-source-file module function)))
			       (my-spec-show (my-alchemist-spec-from-file elixir-source-file module function))
			       )
			   (my-spec-show (format "UNREACHABLE SAUCE FOR %s" expr))
			   )))
		      (t
		       (my-spec-show (format "NO SAUCE FOR %s" expr))))
		))

	(alchemist-server-goto (format "{ \"%s,%s\", [ context: Elixir, imports: %s, aliases: %s ] }" module function modules aliases)
			       #'alchemist-goto-filter)
	)))
    ))


;;(my-alchemist-display-definition "Enum.map")

(defun my-alchemist-spec-at-point ()
  "Display the typespec at the point, if it exists."
  (interactive)
  (my-alchemist-display-definition (alchemist-scope-expression)))

(defun my-alchemist-spec-enable-in-buffer ()
  (interactive)
  "Enable spec display on a buffer."
  (defvar last-post-command-position 0
    "Holds the cursor position from the last run of post-command-hooks.")

  (defvar my-spec-cache ()
    "Buffer-local cache of explored typespecs.")

  (defvar my-spec-timer 0
    "Timer for spec lookup events.")

  (defvar my-spec-current-expression ""
    "Current expression being looked up.")

  (defvar my-spec-lookup-interval 0.3
    "Interval for which to wait before looking up typespec.")

  (make-variable-buffer-local 'last-post-command-position)
  (make-variable-buffer-local 'my-spec-cache)
  (make-variable-buffer-local 'my-spec-timer)
  (make-variable-buffer-local 'my-spec-current-expression)

  (add-to-list
   'post-command-hook
   (lambda ()
     (let ((curexpr (alchemist-scope-expression)))
       (if (not (equal (point) last-post-command-position))
	   (if (timerp my-spec-timer)
	       (cancel-timer my-spec-timer)))

       (setq my-spec-timer (run-with-idle-timer my-spec-lookup-interval nil 'my-alchemist-spec-at-point))
       (setq my-spec-current-expression curexpr)
       (setq last-post-command-position (point))))))

(defun my-alchemist-spec-enable ()
  "Enable spec display on all elixir buffers."
  (add-hook 'elixir-mode-hook 'my-alchemist-spec-enable-in-buffer))

(provide 'alchemist-spec)
;;; alchemist-spec.el ends here
