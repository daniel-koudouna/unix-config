#!/bin/bash

# Remove local files and link config files

ln -sfT $(pwd)/init.el ~/.emacs.d/init.el
ln -sfT $(pwd)/config ~/.emacs.d/config
ln -sfT $(pwd)/settings ~/.emacs.d/settings
ln -sfT $(pwd)/custom-packages ~/.emacs.d/custom-packages
ln -sfT $(pwd)/org-templates ~/.emacs.d/org-templates
