;;; packages -- Emacs initialization
;;; Commentary:
;;; Code:

(defun my:require-and-install (package)
  "Install PACKAGE if not present."
  (unless (package-installed-p package)
    (package-refresh-contents)
    (package-install package))
  (require package))

(setq gc-cons-threshold (* 100 1000 1000))

(require 'package)
(setq package-enable-at-startup nil)

(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(my:require-and-install 'use-package)
(my:require-and-install 'diminish)
(my:require-and-install 'bind-key)

(setq use-package-always-ensure t)

(use-package evil
  :config
  (evil-mode 1)
  (define-key evil-normal-state-map (kbd "j") 'evil-next-visual-line)
  (define-key evil-normal-state-map (kbd "k") 'evil-previous-visual-line)
  (define-key evil-normal-state-map (kbd "C-e") 'evil-end-of-line)
  (define-key evil-normal-state-map (kbd "C-p") 'evil-jump-forward)
  (define-key evil-normal-state-map (kbd "C-o") 'evil-jump-backward)
  (define-key evil-visual-state-map (kbd "C-e") 'evil-end-of-line)
  (define-key evil-normal-state-map (kbd "q") 'avy-goto-char)
  (define-key evil-normal-state-map (kbd "TAB") 'indent-for-tab-command)
  (evil-define-key 'normal org-mode-map (kbd "TAB") #'org-cycle)
  (define-key evil-normal-state-map (kbd "Q")'my:indent-buffer)
  (evil-ex-define-cmd "tj[ump]" #'etags-select-find-tag))

(use-package neotree
  :config
  (setq neo-theme 'arrow)

  (if (display-graphic-p)
      (setq neo-theme 'icons))
  (setq neo-window-width 40)
  (defun t:neotree-theme (_)
    (setq neo-theme 'icons))

  (add-to-list 'after-make-frame-functions #'t:neotree-theme)

  (global-set-key (kbd "C-f") 'neotree-toggle)

  (define-key evil-normal-state-map (kbd "C-f") 'neotree-toggle)
  (define-key evil-insert-state-map (kbd "C-f") 'neotree-toggle)
  (setq neo-vc-integration (quote (face)))


  ;; Ensure neotree always opens the last used window.
  ;; This fixes what I understand is a bug with mru-window, where
  ;; a terminal window would always be replaced.
  (setq my:last-window nil)

  (defun my:neotree-save-last-window (togglefn &rest args)
    (setq my:last-window (selected-window))
    (apply togglefn args))

  (defun my:neotree-select-window (selectfn &rest args)
    (if (bound-and-true-p my:last-window)
        (select-window my:last-window)
      (apply selectfn args)))

  (advice-add 'neotree-toggle :around 'my:neotree-save-last-window)
  (advice-add 'neo-global--select-mru-window :around 'my:neotree-select-window)

  (defun neo-global--select-mru-window (arg)
    "Create or find a window to select when open a file node.
The description of ARG is in `neotree-enter'."
    (when (eq (safe-length (window-list)) 1)
      (neo-buffer--with-resizable-window
       (split-window-horizontally)))
    (when neo-reset-size-on-open
      (neo-global--when-window
        (neo-window--zoom 'minimize)))
    ;; select target window
    (cond
     ;; select window with winum
     ((and (integerp arg)
           (bound-and-true-p winum-mode)
         (fboundp 'winum-select-window-by-number))
      (winum-select-window-by-number arg))
     ;; select window with window numbering
     ((and (integerp arg)
         (boundp 'window-numbering-mode)
         (symbol-value window-numbering-mode)
         (fboundp 'select-window-by-number))
      (select-window-by-number arg))
     ;; open node in a new vertically split window
     ((and (stringp arg) (string= arg "a")
           (fboundp 'ace-select-window))
      (ace-select-window))
     ((and (stringp arg) (string= arg "|"))
      (select-window (get-mru-window))
      (split-window-right)
      (windmove-right))
     ;; open node in a new horizontally split window
     ((and (stringp arg) (string= arg "-"))
      (select-window (get-mru-window))
      (split-window-below)
      (windmove-down)))
    ;; open node in last active window
    (select-window (get-mru-window)))

  (add-hook
   'neotree-mode-hook
   (lambda ()
     (define-key evil-normal-state-local-map (kbd "TAB") 'neotree-enter)
     (define-key evil-normal-state-local-map (kbd "q") 'neotree-hide)
     (define-key evil-normal-state-local-map (kbd "RET") 'neotree-enter)
     (define-key evil-normal-state-local-map (kbd "C-r") 'neotree-change-root)
     )))

(use-package all-the-icons :after neotree)

(use-package which-key
  :config
  (which-key-mode))

(use-package ivy
  :init
  (add-hook 'after-init-hook 'ivy-mode)
  :config
  (define-key ivy-mode-map (kbd "C-j") 'ivy-next-line)
  (define-key ivy-mode-map (kbd "C-k") 'ivy-previous-line))

(use-package avy)

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package company
  :bind
  (("C-SPC" . company-complete))
  :config
  (global-company-mode)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-j") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  (define-key company-active-map (kbd "C-k") 'company-select-previous)
  (define-key company-active-map (kbd "<escape>") 'company-abort))

(use-package multi-term
  :config
  (setq multi-term-dedicated-max-window-height 40)
  (setq multi-term-dedicated-select-after-open-p 't)
  (defun multi-term-dedicated-get-window ()
    "Get `multi-term' dedicated window."
    (setq multi-term-dedicated-window
          (if (> (window-pixel-width) (window-pixel-height))
              (progn
                (split-window-horizontally))
            (progn
              (split-window-vertically)))))

  (setq evil--jumps-buffer-targets "\\*\\(\\new\\|scratch\\|terminal\\|deft\\)\\*")

  (defmacro evil--jumps-message (format &rest args)
    `(with-current-buffer (get-buffer-create "*evil-jumps*")
       (goto-char (point-max))
       (insert (apply #'format ,format ',args) "\n")))


;;  (defun my:multi-term-add-jump (termfn &rest args)
;;    (apply termfn args)
;;    (evil--jumps-push))
;;  (advice-add 'multi-term :around 'my:multi-term-add-jump)

  (setq evil--jumps-buffer-targets "\\*\\(\\new\\|scratch\\|terminal.*\\)\\*")

  (global-set-key (kbd "C-c t") 'multi-term))

(use-package linum
  :config
  (global-linum-mode 1)
  (add-hook 'eshell-mode-hook (lambda() (linum-mode 0)))
  (add-hook 'term-mode-hook (lambda() (linum-mode 0))))

(use-package outshine
  :config
  (defvar outline-minor-mode-prefix "\M-#")
  (add-hook 'emacs-lisp-mode-hook 'outshine-mode))

(use-package yasnippet
  :config
  (yas-global-mode 1))

(use-package flycheck
  :hook (after-init-hook . global-flycheck-mode))

(use-package deft
  :after org
  :bind
  ("C-c d" . deft)
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-directory "~/org/")
  (deft-use-filename-as-title t))

(use-package magit
  :config
  (global-set-key (kbd "C-c g") 'magit-status))

(use-package evil-magit
  :after evil
  :after magit)

;;; packages.el ends here
