;;; options -- Emacs initialization
;;; Commentary:
;;; Code:

(setq backup-directory-alist '((".*" . "~/.saves")))
(setq auto-save-file-name-transforms '((".*" "~/.saves" t)))

(add-to-list 'load-path (expand-file-name "~/.emacs.d/custom-packages"))

(setq custom-file (expand-file-name "~/.emacs.d/custom.el"))

(global-subword-mode t)
(setq-default indent-tabs-mode nil)

;;; options.el ends here
