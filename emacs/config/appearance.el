;;; appearance -- Emacs initialization
;;; Commentary:
;;; Code:

(setq custom-safe-themes t)
(use-package color-theme-sanityinc-tomorrow
  :config
  (color-theme-sanityinc-tomorrow-night))

(set-face-attribute 'default nil :height 140)

(custom-set-faces
 '(org-block-background ((t (:background "gray8"))))
 '(org-block-begin-line ((t (:foreground "DarkSeaGreen3" :weight bold))) t)
 '(org-block-end-line ((t (:foreground "DarkSeaGreen3" :weight bold))) t))


(defun my:run-on-terminal (fn)
  (if (not window-system)
      (funcall fn))
  (add-hook 'after-make-frame-functions
            (lambda()
              (if (not window-system)
                  (funcall fn)))))

(my:run-on-terminal
 (lambda()
   (set-face-attribute 'flycheck-error nil :background "dark red")
   (set-face-attribute 'flycheck-warning nil :background "chocolate4")
   (set-face-attribute 'flycheck-info nil :background "dark green")
   ))


(global-prettify-symbols-mode)

(setq inhibit-startup-screen t)
(setq inhibit-startup-message t)
(delete-other-windows)
(tool-bar-mode -1)
(menu-bar-mode -1)

(setq gc-cons-threshold (* 8 1000 1000))

;;; appearance.el ends here
