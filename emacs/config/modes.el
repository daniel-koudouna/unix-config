;;; modes -- Emacs initialization
;;; Commentary:
;;; Code:

;;; LSP
(use-package lsp-mode
  :config
  (setq lsp-log-io t)
  )

(use-package lsp-ui
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
  (define-key lsp-mode-map (kbd "C-q") 'lsp-execute-code-action)

  (setq lsp-ui-sideline-enable t
        lsp-ui-doc-enable t
        lsp-ui-doc-max-height 30
        lsp-ui-doc-delay 1.0
        lsp-ui-doc-position 'at-point
        lsp-ui-flycheck-enable t
        lsp-ui-imenu-enable t
        lsp-ui-peek-enable t
        lsp-ui-peek-fontify 'always
        lsp-ui-sideline-ignore-duplicate t))

(use-package company-lsp
  :config
  (push 'company-lsp company-backends))

(use-package dap-mode
  :config
  (dap-mode)
  (dap-ui-mode))

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(define-key emacs-lisp-mode-map (kbd "C-c C-c") 'eval-region)

(let ((parent-regex "^\\([^.]\\|\\.[^.]\\|\\.\\..\\)")
      (dir "~/.emacs.d/config/languages/"))
  (mapc 'my:run-safely
        (mapcar (lambda(file) (concat dir file))
                (directory-files (expand-file-name dir) nil parent-regex))))

;;; Elixir

(my:run-shell-script-conditionally
 (lambda() (file-directory-p (expand-file-name "~/.emacs.d/elixir-lsp")))
 "Installing elixir-lsp"

 "wget  https://github.com/elixir-lsp/elixir-ls/archive/v0.3.0.zip -O ~/.emacs.d/elixir-lsp.zip"
 "mkdir ~/.emacs.d/elixir-lsp"
 "unzip ~/.emacs.d/elixir-lsp.zip -d ~/.emacs.d/elixir-lsp"
 "cd ~/.emacs.d/elixir-lsp/elixir-ls-0.3.0 && mix deps.get"
 "cd ~/.emacs.d/elixir-lsp/elixir-ls-0.3.0 && mix compile"
 "cd ~/.emacs.d/elixir-lsp/elixir-ls-0.3.0 && mix elixir_ls.release -o ../")

(add-to-list 'exec-path (expand-file-name "~/.emacs.d/elixir-lsp/"))


(use-package elixir-mode
  :config
  (add-hook
   'elixir-mode-hook
   (lambda()
     (my:prettify
      '("|>" . ?▷)
      '("->" . ?→))))

  (add-hook
   'elixir-mode-hook
   #'lsp))

(require 'dap-elixir)

(use-package alchemist
  :config
  (add-hook
   'elixir-mode-hook
   (lambda()
     (defun alchemist-company (command &optional arg &rest ignored) nil))))

;;; Java
(use-package lsp-java
  :config
  (setq lsp-java-format-settings-url (expand-file-name "~/.emacs.d/settings/java-style.xml")
        lsp-java-format-on-type-enabled nil)
  (add-hook 'java-mode-hook 'lsp))

(add-hook 'java-mode-hook (lambda () (setq tab-width 2)))


(use-package gradle-mode)
(use-package flycheck-gradle)

;;; C++

;;Remember to register your build process with clangd using compile_commands.json
;;$ cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=1 ..
;;$ ln -s ~/myproject/build/compile_commands.json ~/myproject
(if (executable-find "clangd")
    (add-hook 'c++-mode-hook #'lsp)
  (progn
    (my:log-message "\tclangd not found in PATH. Installation requires admin privilages")
    (my:log-message "\tConsult https://apt.llvm.org/ for installation:")
    (my:log-message "\t$ sudo add-apt-repository deb http://apt.llvm.org/xenial/ llvm-toolchain-xenial-8 main")
    (my:log-message "\t$ sudo apt update")
    (my:log-message "\t$ sudo apt-get install clang-tools-8")
    (my:log-message "\t$ sudo update-alternatives --install /usr/bin/clangd clangd /usr/bin/clangd-8 100")
    ))


(add-hook
 'c++-mode-hook
 (lambda()
   (my:prettify
    '("<<<" . ?⟪)
      '(">>>" . ?⟫))
   (define-key evil-normal-state-map "gd" 'lsp-find-definition)
   ))
(add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode))

(use-package cuda-mode)


;;; Python
(use-package python-mode
  :config
  (global-eldoc-mode -1)
  (add-hook 'python-mode-hook #'(lambda () (setq flycheck-checker 'python-flake8)))
  (setq python-indent-guess-indent-offset-verbose nil))

(use-package pyenv-mode
  :config
  (pyenv-mode)
  (pyenv-mode-set "3.6.0")
  (setq lsp-pyls-server-command  '("python3" "-m" "pyls"))
  (add-hook 'python-mode-hook #'lsp))

(use-package company-jedi
  :config
  (add-hook 'python-mode-hook 'jedi:setup)
  (setq jedi:complete-on-dot t))


;;; Ruby

;; rbenv
(setenv "PATH" (concat (getenv "HOME") "/.rbenv/shims:" (getenv "HOME") "/.rbenv/bin:" (getenv "PATH")))
(setq exec-path (cons (concat (getenv "HOME") "/.rbenv/shims") (cons (concat (getenv "HOME") "/.rbenv/bin") exec-path)))

(use-package enh-ruby-mode)

(my:run-shell-script-conditionally
 (lambda() (executable-find "solargraph"))
 "Installing solargraph"
 "gem install --user-install solargraph")

(if (executable-find "solargraph")
    (add-hook 'ruby-mode-hook #'lsp))

;;; Org-roam

(my:run-shell-script-conditionally
 (lambda() (file-directory-p (expand-file-name "~/.emacs.d/org-roam")))
 "Installing org-roam"

 "git clone https://github.com/jethrokuan/org-roam/ ~/.emacs.d/org-roam")

(defun my:org-roam-insert ()
  (interactive)
  (my:insert-after 'org-roam-insert))

(use-package org-roam
  :load-path "~/.emacs.d/org-roam"
  :after org
  :hook (org-mode . org-roam-mode)
  :custom
  (org-roam-directory "~/Dropbox/org/roam/")
  :bind
  ("C-c n l" . org-roam)
  ("C-c n t" . org-roam-today)
  ("C-c n f" . org-roam-find-file)
  ("C-c n i" . my:org-roam-insert)
  ("C-c n g" . org-roam-show-graph))

;;; modes.el ends here
