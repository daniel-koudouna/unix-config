;;; latex -- Emacs initialization
;;; Commentary:
;;; Code:

(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-save-query nil)
(setq ispell-program-name "aspell")
(setq ispell-dictionary "english")

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)

(setq LaTeX-command-style '(("" "%(PDF)%(latex) -shell-escape %S%(PDFout)")))

(add-hook 'LaTeX-mode-hook
          (lambda ()
            (add-to-list
             'TeX-command-list
             '("latexmk" "latexmk -interaction=nonstopmode -shell-escape -pdf %s" TeX-run-TeX nil t
               :help "Run latexmk on file"))
            (setq TeX-command-default "latexmk")))

(setq-default TeX-master nil) ; Query for master file.

(use-package reftex
  :after evil
  :config
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)

  (defun my:reftex-reference ()
    "Custom reftex to ensure references appear at the end of words."
    (interactive)
    (my:insert-after 'reftex-reference))

  (defun my:reftex-citation ()
    "Custom reftex to ensure citations appear at the end of words."
    (interactive)
    (my:insert-after 'reftex-citation))

  (defun my:is-end-of-line ()
    "Compare point with end of line."
    (let* ((pos (current-column))
           (end-pos (save-excursion
                      (evil-end-of-line)
                      (current-column))))
      (eq pos end-pos)))

  (defun my:compare-with-end-of-word ()
    "Compare point with end of word."
    (let* ((pos (current-column))
           (end-pos (save-excursion
                      (evil-backward-word-begin)
                      (evil-forward-word-end)
                      (current-column))))
      (- pos end-pos)))

  (defun my:point-is-space ()
    "Check if point is whitespace."
    (char-equal ?\s (char-after)))

  (defun my:insert-after (func)
    "Run FUNC after the end of word, ignoring whitespace."
    (interactive)
    (let ((relative-loc (my:compare-with-end-of-word)))
      (cond ((my:is-end-of-line)
             (end-of-line)
             (call-interactively func))
            ((eq 0 relative-loc)
             (evil-forward-char)
             (call-interactively func))
            ((and (> 0 relative-loc) (not (my:point-is-space)))
             (evil-forward-word-end)
             (if (my:is-end-of-line)
                 (end-of-line)
               (evil-forward-char))
             (call-interactively func))
            (t
             (call-interactively func)))))

  (evil-define-key 'normal reftex-mode-map (kbd "C-c )") 'my:reftex-reference)
  (evil-define-key 'normal reftex-mode-map (kbd "C-c [") 'my:reftex-citation)

  (setq reftex-default-bibliography '("~/.config/tex/bibliography.bib"))
  (setq reftex-plug-into-AUCTeX t))

(setq org-export-with-title nil)


(evil-define-key 'normal emacs-lisp-mode-map (kbd "C-c '") 'org-edit-src-exit)
(evil-define-key 'normal latex-mode-map (kbd "C-c '") 'org-edit-src-exit)

;;; latex.el ends here
