;;; org -- Emacs initialization
;;; Commentary:
;;; Code:

(use-package org
  :mode
  ("\\.org" . org-mode)
  :config
  (setq org-latex-listings t)
  (setq org-agenda-files (my:find-org-folders "~/org"))
  (setq org-src-fontify-natively t)
  (setq org-enforce-todo-dependencies t)
  (define-key org-src-mode-map (kbd "C-c '") 'org-edit-src-exit)

  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.4))

  (setq org-agenda-prefix-format
        '((agenda . " %i %-12t% s")
          (timeline . "  % s")
          (todo . " %i %12:c ")
          (tags . " %i %12:c ")
          (search . " %i %12:c ")))

  (setq org-latex-listings 'minted)
  (setq org-latex-pdf-process
        '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

  (setq org-fontify-quote-and-verse-blocks t)

  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (ruby . t)))

  (global-set-key (kbd  "C-c a") 'org-agenda)

  (setq org-agenda-custom-commands
        '(("s" todo "TODO|DONE")
          ("d" "Daily Planner"
           ((agenda "")
            (alltodo)))
          ))

  (add-hook
   'org-agenda-mode-hook
   (lambda ()
     (define-key org-agenda-mode-map "j" 'org-agenda-next-line)
     (define-key org-agenda-mode-map "k" 'org-agenda-previous-line)
     ))

  (setq browse-url-browser-function 'eww-browse-url)
  (evil-define-key 'normal eww-mode-map "q" 'delete-window)
  (defun lookup-thing-at-point ()
    (interactive)
    (let ((word (thing-at-point 'symbol)))
      (split-window-horizontally)
      (other-window 1)
      (browse-url (concat "http://webstersdictionary1828.com/Dictionary/" word))
      (search-forward word)
      ))

  (add-hook
   'org-mode-hook
   (lambda ()
     (evil-define-key 'normal org-mode-map "gd" 'org-roam-find-at-point)
     (evil-define-key 'normal org-mode-map "gf" 'lookup-thing-at-point)
     (define-key org-mode-map (kbd "C-a") 'evil-beginning-of-line)
     (visual-line-mode)
     (org-indent-mode)))

  (setq org-latex-prefer-user-labels t)
  (use-package org-ref
    :after org
    :config
    (setq org-ref-default-bibliography '("~/.config/tex/bibliography.bib")))


  (use-package org-bullets
    :config (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

  (defun my:publish ()
    (interactive)
    (load (expand-file-name "~/.emacs.d/custom-packages/blog.el"))
    (call-interactively 'org-publish))

  )
;;; org.el ends here
