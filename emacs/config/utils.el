;;; utils -- Emacs Configuration
;;; Commentary:
;;; Code:

(defun config()
  (interactive)
  (find-file (expand-file-name "~/.emacs.d/init.el")))

(defun reconfig()
  (interactive)
  (load-file  (expand-file-name "~/.emacs.d/init.el")))

(defun my:indent-buffer()
  (interactive)
  (indent-region (point-min) (point-max)))

(defun my:prettify (&rest symbols)
  "Add all SYMBOLS to 'prettify-symbols-alist'."
  (mapc (lambda (sym) (push sym prettify-symbols-alist)) symbols))

(defun my:run-shell-script (script)
  "Run SCRIPT on shell."
  (shell-command (format "bash -c %s" (shell-quote-argument script))))

(defun my:run-shell-script-conditionally (condition title &rest commands)
  (if (not (funcall condition))
      (progn
        (my:log-message (concat "\t" title))
        (mapc (lambda(command)
                (my:log-message (concat "\t\t" command))
                (my:run-shell-script command))
              commands)
        (if (funcall condition)
            (my:log-message "\tSuccess")
          (my:log-message "\tFailure"))
        )))

(defun my:find-org-folders (root)
  "Find all folders in directory ROOT."
  (append
   (list (expand-file-name root))
   (my:filter 'file-directory-p (directory-files-recursively root "" t))))

(defun my:filter (func l)
  "Use FUNC to filter list L."
  (delete
   "e3824adc733a"
   (mapcar (lambda (x) (if (funcall func x) x "e3824adc733a")) l)))

(add-hook
 'emacs-startup-hook
 (lambda ()
   (let* ((delta-time (time-subtract after-init-time before-init-time))
          (time-taken (format "%.2f seconds" (float-time delta-time)))
          (msg (format "Emacs Ready in %s with %d GC cycles."
                       time-taken gcs-done)))
     (my:log-message msg))))

;;; utils.el ends here
