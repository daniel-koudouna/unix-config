;;; init -- Emacs initialization
;;; Commentary:
;;; Code:


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;; (package-initialize)

(defun my:log-message (string)
  "Logs STRING to the scratch buffer."
  (with-current-buffer "*scratch*"
    (goto-char (point-max))
    (insert "\n;; ")
    (insert string)))

(defun my:run-safely (file)
  "Evaluate elisp FILE and report any errors."
  (condition-case err
      (progn
        (load-file (expand-file-name file))
        (my:log-message (format "[ OK  ] %s" file)))
    (error
     (let ((err-message (error-message-string err)))
       (let ((failure-message (format "[Error] %s" file))
             (error-message (format "\t%s" err-message)))
         (my:log-message failure-message)
         (my:log-message error-message)
         )))))

(setq custom-file (expand-file-name "~/.emacs.d/custom.el"))
(setq vc-follow-symlinks t)
(my:log-message "----------Begin configuration")
(my:run-safely "~/.emacs.d/config/options.el")
(my:run-safely "~/.emacs.d/config/utils.el")
(my:run-safely "~/.emacs.d/config/packages.el")
(my:run-safely "~/.emacs.d/config/modes.el")
(my:run-safely "~/.emacs.d/config/appearance.el")
(setq vc-follow-symlinks nil)
(my:log-message "----------End configuration")

;;; init.el ends here
